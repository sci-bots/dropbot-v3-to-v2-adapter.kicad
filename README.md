# DropBot v3-v2 adapter board

This adapter allows you to connect a [DropBot v2 device connector](http://microfluidics.utoronto.ca/trac/dropbot/wiki/120ChannelDeviceConnector) to a DropBot v3 instrument.

## !!! *DISCLAIMER* !!!

**This design is provided as is with no warranty or guarantees. It bypasses the DropBot's hardware interlock (a safety feature designed to prevent the DropBot from applying high-voltage without a chip present) and has not been tested for electromagnetic compliance. No responsibility is accepted for any damage, injury or death as a result of building/using this design. Any damage to your DropBot that occurs as a result of this adapter will not be covered by the Sci-Bots Inc. warranty.**

![image](/uploads/d61237de4688795cd5eef1503ca1bea3/image.png)

![image](/uploads/c0566b81f8b15a11b3fb9b4c16c1c5c5/image.png)
